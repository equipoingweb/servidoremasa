/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.service;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import model.Aviso;

/**
 *
 * @author Carlos
 */
@Stateless
@Path("model.aviso")
public class AvisoFacadeREST extends AbstractFacade<Aviso> {

    @PersistenceContext(unitName = "ServidorEMASAPU")
    private EntityManager em;

    public AvisoFacadeREST() {
        super(Aviso.class);
    }

     @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Aviso entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Aviso entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Aviso find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Path("/buscar/email/{email}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Aviso> findEmail(@PathParam("email") String email) {
        return buscarAvisoPorEmail(email);
    }
    
     @GET
    @Path("/buscar/palabras/{clave}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Aviso> findPalabrasClave(@PathParam("clave") String clave) {
        String[] splited = clave.split("\\s+");
        return buscarAvisoPorPalabras(splited);
    }
    
    @GET
    @Path("/buscar/abiertos")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Aviso> findAbiertos() {
        return buscarAvisosAbiertos();
    }
    
    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Aviso> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Aviso> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

      public List<Aviso> buscarAvisoPorPalabras(String[] palabras){
        String where = "";
        if (palabras.length>0){
            where = "WHERE a.descripcion LIKE '%"+palabras[0]+"%'";
            
        }
        for (int i = 1 ; i<palabras.length; i++){
            where += "AND a.descripcion LIKE '%"+palabras[i]+"%'";
        }
        javax.persistence.Query q = getEntityManager().createQuery("SELECT a FROM Aviso a "+where);
        return q.getResultList();
    }
    
    public List<Aviso> buscarAvisoPorEmail(String mail){
      
        javax.persistence.Query q = getEntityManager().createNamedQuery("Aviso.findByEmailUsuario");
        q.setParameter("emailUsuario", mail);
        return q.getResultList();
    }
    public List<Aviso> buscarPorProximidad(String ubicacion){
        List<Aviso> todos = findAll();
       
        return todos;
    }
    
    public List<Aviso> buscarAvisosAbiertos(){
        Query q;
        q = em.createQuery("SELECT a FROM Aviso a WHERE a.estado NOT LIKE :ESTADO");
        q.setParameter("ESTADO", "Cerrado");
        
        return q.getResultList();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
