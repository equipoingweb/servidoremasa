/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Carlos
 */
@Entity
@Table(name = "aviso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aviso.findAll", query = "SELECT a FROM Aviso a")
    , @NamedQuery(name = "Aviso.findByIdAviso", query = "SELECT a FROM Aviso a WHERE a.idAviso = :idAviso")
    , @NamedQuery(name = "Aviso.findByFechaCreacion", query = "SELECT a FROM Aviso a WHERE a.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "Aviso.findByFechaCierre", query = "SELECT a FROM Aviso a WHERE a.fechaCierre = :fechaCierre")
    , @NamedQuery(name = "Aviso.findByLongitud", query = "SELECT a FROM Aviso a WHERE a.longitud = :longitud")
    , @NamedQuery(name = "Aviso.findByLatitud", query = "SELECT a FROM Aviso a WHERE a.latitud = :latitud")
    , @NamedQuery(name = "Aviso.findByTipo", query = "SELECT a FROM Aviso a WHERE a.tipo = :tipo")
    , @NamedQuery(name = "Aviso.findByUrgencia", query = "SELECT a FROM Aviso a WHERE a.urgencia = :urgencia")
    , @NamedQuery(name = "Aviso.findByEstado", query = "SELECT a FROM Aviso a WHERE a.estado = :estado")
    , @NamedQuery(name = "Aviso.findByIdUsuario", query = "SELECT a FROM Aviso a WHERE a.idUsuario = :idUsuario")
    , @NamedQuery(name = "Aviso.findByEmailUsuario", query = "SELECT a FROM Aviso a WHERE a.emailUsuario = :emailUsuario")})
public class Aviso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idAviso")
    private Integer idAviso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaCreacion")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;
    @Column(name = "fechaCierre")
    @Temporal(TemporalType.DATE)
    private Date fechaCierre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "longitud")
    private int longitud;
    @Basic(optional = false)
    @NotNull
    @Column(name = "latitud")
    private int latitud;
    @Lob
    @Size(max = 65535)
    @Column(name = "urlMaps")
    private String urlMaps;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "tipo")
    private String tipo;
    @Lob
    @Size(max = 65535)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "urgencia")
    private int urgencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "estado")
    private String estado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idUsuario")
    private int idUsuario;
    @Size(max = 45)
    @Column(name = "emailUsuario")
    private String emailUsuario;
    @JoinTable(name = "relacionaviso", joinColumns = {
        @JoinColumn(name = "idAvisoBase", referencedColumnName = "idAviso")}, inverseJoinColumns = {
        @JoinColumn(name = "idAvisoRepetido", referencedColumnName = "idAviso")})
    @ManyToMany
    private Collection<Aviso> avisoCollection;
    @ManyToMany(mappedBy = "avisoCollection")
    private Collection<Aviso> avisoCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAviso")
    private Collection<Adjunto> adjuntoCollection;
    @OneToMany(mappedBy = "idAviso")
    private Collection<Operacion> operacionCollection;

    public Aviso() {
    }

    public Aviso(Integer idAviso) {
        this.idAviso = idAviso;
    }

    public Aviso(Integer idAviso, Date fechaCreacion, int longitud, int latitud, String tipo, int urgencia, String estado, int idUsuario) {
        this.idAviso = idAviso;
        this.fechaCreacion = fechaCreacion;
        this.longitud = longitud;
        this.latitud = latitud;
        this.tipo = tipo;
        this.urgencia = urgencia;
        this.estado = estado;
        this.idUsuario = idUsuario;
    }

    public Integer getIdAviso() {
        return idAviso;
    }

    public void setIdAviso(Integer idAviso) {
        this.idAviso = idAviso;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(Date fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    public int getLatitud() {
        return latitud;
    }

    public void setLatitud(int latitud) {
        this.latitud = latitud;
    }

    public String getUrlMaps() {
        return urlMaps;
    }

    public void setUrlMaps(String urlMaps) {
        this.urlMaps = urlMaps;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getUrgencia() {
        return urgencia;
    }

    public void setUrgencia(int urgencia) {
        this.urgencia = urgencia;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getEmailUsuario() {
        return emailUsuario;
    }

    public void setEmailUsuario(String emailUsuario) {
        this.emailUsuario = emailUsuario;
    }

    @XmlTransient
    public Collection<Aviso> getAvisoCollection() {
        return avisoCollection;
    }

    public void setAvisoCollection(Collection<Aviso> avisoCollection) {
        this.avisoCollection = avisoCollection;
    }

    @XmlTransient
    public Collection<Aviso> getAvisoCollection1() {
        return avisoCollection1;
    }

    public void setAvisoCollection1(Collection<Aviso> avisoCollection1) {
        this.avisoCollection1 = avisoCollection1;
    }

    @XmlTransient
    public Collection<Adjunto> getAdjuntoCollection() {
        return adjuntoCollection;
    }

    public void setAdjuntoCollection(Collection<Adjunto> adjuntoCollection) {
        this.adjuntoCollection = adjuntoCollection;
    }

    @XmlTransient
    public Collection<Operacion> getOperacionCollection() {
        return operacionCollection;
    }

    public void setOperacionCollection(Collection<Operacion> operacionCollection) {
        this.operacionCollection = operacionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAviso != null ? idAviso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aviso)) {
            return false;
        }
        Aviso other = (Aviso) object;
        if ((this.idAviso == null && other.idAviso != null) || (this.idAviso != null && !this.idAviso.equals(other.idAviso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Aviso[ idAviso=" + idAviso + " ]";
    }
    
}
